package rajiv.com.mapproject.activities;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import rajiv.com.mapproject.R;
import rajiv.com.mapproject.application_utils.ImageInputHelper;
import rajiv.com.mapproject.application_utils.Permission.CheckPermissionUtil;
import rajiv.com.mapproject.application_utils.Permission.PermissionUtil;
import rajiv.com.mapproject.application_utils.Utils;
import rajiv.com.mapproject.constants.IntentConstant;
import rajiv.com.mapproject.framework.initialize.ActivityInitialize;
import rajiv.com.mapproject.model.MarkerModel;

import static rajiv.com.mapproject.application_utils.Permission.CheckPermissionUtil.WRITE_SD_REQ_CODE;
import static rajiv.com.mapproject.constants.BundleConstant.BUNDLE_ITEM_SELECTED;


public class MapsActivity extends FragmentActivity implements ActivityInitialize, View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, ImageInputHelper.ImageActionListener {

    private static final String TAG = MapsActivity.class.getSimpleName();

    private GoogleMap mMap;
    private FloatingActionButton mFloatingActionButton;
    private ImageInputHelper imageInputHelper;
    private Realm realmDB;
    private Geocoder geocoder;
    private MarkerModel currentMarkerModel;
    private List<MarkerModel> markerModels;
    HashMap<Integer, Marker> hashMapMarker = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Realm.init(this);
        realmDB = Realm.getDefaultInstance();

        CheckPermissionUtil.checkWriteSd(this,
                new PermissionUtil.ReqPermissionCallback() {
                    @Override
                    public void onResult(boolean success) {
                    }
                }, "We need storage permission to take photos."
                , "We can't take photos without storage permission");

        initialize();


    }


    private void save(final double latitude, final double longitude, final String uri) {

        realmDB.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                Number currentIdNum = realm.where(MarkerModel.class).max("ID");
                int nextId;
                if (currentIdNum == null) {
                    nextId = 0;
                } else {
                    nextId = currentIdNum.intValue() + 1;
                }

                MarkerModel markerModel = realm.createObject(MarkerModel.class, nextId);
                markerModel.setLatitude(latitude);
                markerModel.setLongitude(longitude);
                markerModel.setAddress(getAddress(latitude, longitude));
                markerModel.setPhotoUri(uri);


                currentMarkerModel = realm.copyFromRealm(markerModel);


            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

                mMap.addMarker(new MarkerOptions().position(new LatLng(currentMarkerModel.getLatitude(), currentMarkerModel.getLongitude())));

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {

                Toast.makeText(MapsActivity.this, "Error occurred in transaction", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void load() {


        realmDB.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                RealmResults<MarkerModel> realmResults = realm
                        .where(MarkerModel.class)
                        .findAll();

                markerModels = realm.copyFromRealm(realmResults);


            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

                addMarkers(markerModels);

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.d(TAG, "Error occurred in transaction");

            }
        });

    }


    private void addMarkers(List<MarkerModel> markerModels) {
        mMap.clear();
        for (MarkerModel markerModel : markerModels) {

            Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(markerModel.getLatitude(), markerModel.getLongitude())));
            hashMapMarker.put(markerModel.getID(), marker);

        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        googleMap.setOnMarkerClickListener(this);

        load();

        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                Toast.makeText(MapsActivity.this, "Long: " +
                                latLng.latitude + ", " + latLng.longitude,
                        Toast.LENGTH_SHORT).show();

                currentMarkerModel = new MarkerModel();
                currentMarkerModel.setLatitude(latLng.latitude);
                currentMarkerModel.setLongitude(latLng.longitude);

                Utils.checkPhotoPermission(MapsActivity.this, imageInputHelper, generate(), generate());

            }
        });

    }

    public static String generate() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }

        return buffer.toString();
    }

    @Override
    public void initialize() {

        imageInputHelper = new ImageInputHelper(this);
        imageInputHelper.setImageActionListener(this);
        geocoder = new Geocoder(this, Locale.getDefault());

        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        clickHandler();
    }

    private void clickHandler() {
        mFloatingActionButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fab:

                Intent intent = new Intent(this, ListActivity.class);
                startActivityForResult(intent, IntentConstant.REQUEST_LIST_ITEM);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        PermissionUtil.onRequestPermissionResult(this, requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == WRITE_SD_REQ_CODE) {

            PermissionUtil.onActivityResult(this, requestCode);

        }

        if (requestCode == IntentConstant.REQUEST_LIST_ITEM && resultCode == RESULT_OK) {
            try {
                int selectedPosition = data.getExtras().getInt(BUNDLE_ITEM_SELECTED);
                Marker marker = hashMapMarker.get(selectedPosition);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        imageInputHelper.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        LatLng latLng = marker.getPosition();

        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra(BUNDLE_ITEM_SELECTED, latLng);

        startActivityForResult(intent, IntentConstant.REQUEST_LIST_ITEM);

        return false;
    }

    @Override
    public void onImageSelectedFromGallery(Uri uri, File imageFile) {
        save(currentMarkerModel.getLatitude(), currentMarkerModel.getLongitude(), uri.toString());
    }

    @Override
    public void onImageTakenFromCamera_24_Greater(Uri uri, File imageFile) {
        save(currentMarkerModel.getLatitude(), currentMarkerModel.getLongitude(), uri.toString());
    }

    @Override
    public void onImageTakenFromCamera(int requestCode, int resultCode, Intent data) {
        Uri uri = ImageInputHelper.getPickImageResultUri(this, data);
        save(currentMarkerModel.getLatitude(), currentMarkerModel.getLongitude(), uri.toString());
    }

    @Override
    public void onImageCropped(Uri uri, File imageFile) {
        save(currentMarkerModel.getLatitude(), currentMarkerModel.getLongitude(), uri.toString());
    }

    private String getAddress(double latitude, double longitude) {
        List<Address> addressList = null;
        try {

            StringBuilder stringBuilder = new StringBuilder();

            addressList = geocoder.getFromLocation(latitude, longitude, 1);
            stringBuilder.append(addressList.get(0).getAddressLine(0) + ", ");
            stringBuilder.append(addressList.get(0).getLocality() + ", ");
            stringBuilder.append(addressList.get(0).getCountryName());
            return stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "No address";
    }


}
