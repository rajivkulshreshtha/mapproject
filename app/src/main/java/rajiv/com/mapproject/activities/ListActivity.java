package rajiv.com.mapproject.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rajiv.com.mapproject.R;
import rajiv.com.mapproject.adapters.ItemAdapter;
import rajiv.com.mapproject.framework.initialize.ActivityInitialize;
import rajiv.com.mapproject.model.MarkerModel;

import static rajiv.com.mapproject.constants.BundleConstant.BUNDLE_ITEM_SELECTED;

public class ListActivity extends AppCompatActivity implements ActivityInitialize, ItemAdapter.ItemAdapterCallback {


    private static final String TAG = ListActivity.class.getSimpleName();

    private Realm realmDB;
    private RecyclerView recyclerView;
    private ItemAdapter itemAdapter;
    private LatLng selectedLatLng;
    private List<MarkerModel> markerModels;
    private LinearLayoutManager linearLayoutManager;
    private int selectedPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Realm.init(this);
        realmDB = Realm.getDefaultInstance();

        initialize();

    }

    @Override
    public void initialize() {
        load();
    }

    private void load() {

        realmDB.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                RealmResults<MarkerModel> realmResults = realm
                        .where(MarkerModel.class)
                        .findAll();

                markerModels = realm.copyFromRealm(realmResults);


            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                initRecyclerView(markerModels);

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.d(TAG, "Error occurred in transaction");

            }
        });
    }


    private void initRecyclerView(List<MarkerModel> markerModels) {

        recyclerView = findViewById(R.id.activity_list_recyclerView);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        itemAdapter = new ItemAdapter(this, markerModels);
        itemAdapter.setItemAdapterCallback(this);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.notifyDataSetChanged();

        if (getIntent().hasExtra(BUNDLE_ITEM_SELECTED)) {
            selectedLatLng = getIntent().getParcelableExtra(BUNDLE_ITEM_SELECTED);
            getSelectedPosition(selectedLatLng);
        }

    }


    private void getSelectedPosition(final LatLng latLng) {

        realmDB.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                MarkerModel selectMarkerModel = realm
                        .where(MarkerModel.class)
                        .equalTo("latitude", latLng.latitude)
                        .equalTo("longitude", latLng.longitude)
                        .findFirst();

                selectedPosition = selectMarkerModel.getID();

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                linearLayoutManager.scrollToPosition(selectedPosition);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.d(TAG, "Error occurred in transaction");

            }
        });
    }


    @Override
    public void itemSelected(final int position) {

        final Intent returnIntent = new Intent();

        realmDB.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                MarkerModel selectMarkerModel = realm
                        .where(MarkerModel.class)
                        .equalTo("ID", position)
                        .findFirst();

                selectedPosition = selectMarkerModel.getID();


            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                returnIntent.putExtra(BUNDLE_ITEM_SELECTED, selectedPosition);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.d(TAG, "Error occurred in transaction");

            }
        });

    }
}
