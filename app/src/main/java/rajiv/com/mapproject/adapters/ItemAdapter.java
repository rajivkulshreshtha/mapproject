package rajiv.com.mapproject.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import rajiv.com.mapproject.R;
import rajiv.com.mapproject.model.MarkerModel;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemAdapterViewHolder> {

    private Context context;
    private View view;
    private LayoutInflater layoutInflater;
    private List dataList;

    //Communicator
    private ItemAdapterCallback itemAdapterCallback;

    public ItemAdapter(Context context, List dataList) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.dataList = dataList;
    }

    @Override
    public ItemAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = layoutInflater.inflate(R.layout.adapter_item, parent, false);
        return new ItemAdapterViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ItemAdapterViewHolder holder, int position) {
        final MarkerModel markerModel = (MarkerModel) dataList.get(holder.getAdapterPosition());

        holder.latTextView.setText("Latitude: " + markerModel.getLatitude());
        holder.longTextView.setText("Longitude: " + markerModel.getLongitude());
        holder.addressTextView.setText("Address: " + markerModel.getAddress());

        Glide.with(context)
                .load(Uri.parse(markerModel.getPhotoUri()))
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.imageView);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemAdapterCallback.itemSelected(holder.getAdapterPosition());

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ItemAdapterViewHolder extends RecyclerView.ViewHolder {

        View view;
        ImageView imageView;
        TextView longTextView, latTextView, addressTextView;


        public ItemAdapterViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            imageView = view.findViewById(R.id.imageView);
            longTextView = view.findViewById(R.id.longTextView);
            latTextView = view.findViewById(R.id.latTextView);
            addressTextView = view.findViewById(R.id.addressTextView);

        }
    }

    public void setItemAdapterCallback(ItemAdapterCallback itemAdapterCallback) {
        this.itemAdapterCallback = itemAdapterCallback;
    }

    public interface ItemAdapterCallback {

        void itemSelected(int position);

    }


}
