package rajiv.com.mapproject.framework.initialize;

public interface ActivityInitialize {

    void initialize();
}
