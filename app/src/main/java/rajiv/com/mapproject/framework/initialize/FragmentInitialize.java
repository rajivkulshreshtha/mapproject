package rajiv.com.mapproject.framework.initialize;

import android.view.View;

/**
 * Created by RajivLL on 23-Mar-18.
 */

public interface FragmentInitialize {

    void initialize(View view);

}
