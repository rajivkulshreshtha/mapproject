package rajiv.com.mapproject.application;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by RajivLL on 29-Aug-18.
 */

public class MyApplication extends Application {

    public static final String APPLICATION_ID = "gsi.lastlocal.in.gsi";

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("myDatabase.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfig);
    }

}
