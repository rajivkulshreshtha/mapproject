package rajiv.com.mapproject.application_utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import rajiv.com.mapproject.application.MyApplication;

/**
 * Class that helps, for Android App, selecting image from gallery, getting image from camera and
 * cropping image.
 * <p/>
 * <p/>
 * IMPORTANT: The Activity, that contains this object or that contains the fragment that uses this
 * object, must handle the orientation change during taking photo. Either lock the orientation of
 * the Activity or handle orientation changes. Otherwise taking photo feature will not work since
 * the new instance of this object will be created when device rotates.
 */
public class ImageInputHelper {

    public static final int REQUEST_PICTURE_FROM_GALLERY = 23;
    public static final int REQUEST_PICTURE_FROM_CAMERA = 24;
    public static final int REQUEST_CROP_PICTURE = 25;
    private static final String TAG = "ImageInputHelper";

    private File tempFileFromSource = null;
    private Uri tempUriFromSource = null;

    private File tempFileFromCrop = null;
    private Uri tempUriFromCrop = null;

    private static String imageId, imageTypeId;

    String imageFilePath;

    /*private static ImageInputHelper imageInputHelper;

    public static ImageInputHelper getInstance(Activity context) {
        if (imageInputHelper == null) {
            imageInputHelper = new ImageInputHelper(context);
        }
        return imageInputHelper;
    }

    public static ImageInputHelper getInstance(Fragment fragment) {
        if (imageInputHelper == null) {
            imageInputHelper = new ImageInputHelper((fragment));
        }
        return imageInputHelper;
    }*/

    /**
     * Activity object that will be used while calling startActivityForResult(). Activity then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImageInputHelper for handling result and being notified.
     */
    private Activity mContext;

    /**
     * Fragment object that will be used while calling startActivityForResult(). Fragment then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImageInputHelper for handling result and being notified.
     */
    private Fragment fragment;

    /**
     * Listener instance for callbacks on user events. It must be set to be able to use
     * the ImageInputHelper object.
     */
    private ImageActionListener imageActionListener;

    public ImageInputHelper(Activity mContext) {
        this.mContext = mContext;
    }

    public ImageInputHelper(Fragment fragment) {
        this.fragment = fragment;
        this.mContext = fragment.getActivity();
    }

    public void setImageActionListener(ImageActionListener imageActionListener) {
        this.imageActionListener = imageActionListener;
    }

    /**
     * Handles the result of events that the Activity or Fragment receives on its own
     * onActivityResult(). This method must be called inside the onActivityResult()
     * of the container Activity or Fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == REQUEST_PICTURE_FROM_GALLERY) && (resultCode == Activity.RESULT_OK)) {

            Log.d(TAG, "Image selected from gallery");
            imageActionListener.onImageSelectedFromGallery(data.getData(), tempFileFromSource);

        } else if ((requestCode == REQUEST_PICTURE_FROM_CAMERA) && (resultCode == Activity.RESULT_OK)) {

            Log.d(TAG, "Image selected from camera: " + tempFileFromSource);
            //imageActionListener.onImageTakenFromCamera_24_Greater(tempUriFromSource, tempFileFromSource);
            //imageActionListener.onImageTakenFromCamera(requestCode, resultCode, data);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                imageActionListener.onImageTakenFromCamera_24_Greater(tempUriFromSource, tempFileFromSource);
            } else {
                imageActionListener.onImageTakenFromCamera(requestCode, resultCode, data);
            }

        } else if ((requestCode == REQUEST_CROP_PICTURE) && (resultCode == Activity.RESULT_OK)) {

            Log.d(TAG, "Image returned from crop");
            imageActionListener.onImageCropped(tempUriFromCrop, tempFileFromCrop);
        }
    }

    /**
     * Starts an intent for selecting image from gallery. The result is returned to the
     * onImageSelectedFromGallery() method of the ImageSelectionListener interface.
     */
    public void selectImageFromGallery() {
        checkListener();

        if (tempFileFromSource == null) {
            try {
                tempFileFromSource = File.createTempFile("choose", "png", mContext.getExternalCacheDir());
                // tempUriFromSource = Uri.fromFile(tempFileFromSource);
                tempUriFromSource = FileProvider.getUriForFile(mContext,
                        MyApplication.APPLICATION_ID + ".provider",
                        tempFileFromSource);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUriFromSource);
        if (fragment == null) {
            mContext.startActivityForResult(intent, REQUEST_PICTURE_FROM_GALLERY);
        } else {
            fragment.startActivityForResult(intent, REQUEST_PICTURE_FROM_GALLERY);
        }
    }

    /**
     * Starts an intent for taking photo with camera. The result is returned to the
     * onImageTakenFromCamera_24_Greater() method of the ImageSelectionListener interface.
     */
    public void takePhotoWithCamera(String imageId, String imageTypeId) {
        checkListener();
        Log.d(TAG, "takePhotoWithCamera: ");
        this.imageId = imageId;
        this.imageTypeId = imageTypeId;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUriFromSource);
        if (fragment == null) {
            mContext.startActivityForResult(getFromCamera(mContext), REQUEST_PICTURE_FROM_CAMERA);
        } else {
            fragment.startActivityForResult(getFromCamera(fragment.getActivity()), REQUEST_PICTURE_FROM_CAMERA);
        }

    }


    public void takePhotoWithCamera_24_Greater(String imageId, String imageTypeId) {
        Log.d(TAG, "takePhotoWithCamera_24_Greater: ");
        this.imageId = imageId;
        this.imageTypeId = imageTypeId;

        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile_24_Greater(imageId, imageTypeId);

            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                tempFileFromSource = photoFile;
                tempUriFromSource = photoURI;
                mContext.startActivityForResult(pictureIntent,
                        REQUEST_PICTURE_FROM_CAMERA);
            }
        }
    }

    private File createImageFile_24_Greater(String imageId, String imageTypeId) throws IOException {

        Log.d(TAG, "createImageFile_24_Greater: ");

        /*String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());*/

        String imageFileName = imageId + "_" + imageTypeId + "_" + "ImagePicked";
        File storageDir =
                mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,   //prefix
                ".jpg",   //suffix
                storageDir      //directory
        );

        imageFilePath = image.getAbsolutePath();
        return image;

    }


    /**
     * Starts an intent for cropping an image that is saved in the uri. The result is
     * returned to the onImageCropped() method of the ImageSelectionListener interface.
     *
     * @param uri     uri that contains the data of the image to crop
     * @param outputX width of the result image
     * @param outputY height of the result image
     * @param aspectX horizontal ratio value while cutting the image
     * @param aspectY vertical ratio value of while cutting the image
     */
    public void requestCropImage(Uri uri, int outputX, int outputY, int aspectX, int aspectY) {
        checkListener();

        if (tempFileFromCrop == null) {
            try {
                tempFileFromCrop = File.createTempFile("crop", "png", mContext.getExternalCacheDir());
                //tempUriFromCrop = Uri.fromFile(tempFileFromCrop);
                tempUriFromCrop = FileProvider.getUriForFile(mContext,
                        MyApplication.APPLICATION_ID + ".provider", new File(tempFileFromCrop.getPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // open crop intent when user selects image
        final Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("output", tempUriFromCrop);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("aspectX", aspectX);
        intent.putExtra("aspectY", aspectY);
        intent.putExtra("scale", true);
        intent.putExtra("noFaceDetection", true);
        if (fragment == null) {
            mContext.startActivityForResult(intent, REQUEST_CROP_PICTURE);
        } else {
            fragment.startActivityForResult(intent, REQUEST_CROP_PICTURE);
        }
    }

    private void checkListener() {
        if (imageActionListener == null) {
            throw new RuntimeException("ImageSelectionListener must be set before calling openGalleryIntent(), takePhotoWithCamera_24_Greater() or requestCropImage().");
        }
    }

    /**
     * Listener interface for receiving callbacks from the ImageInputHelper.
     */
    public interface ImageActionListener {

        void onImageSelectedFromGallery(Uri uri, File imageFile);

        void onImageTakenFromCamera_24_Greater(Uri uri, File imageFile);

        void onImageTakenFromCamera(int requestCode, int resultCode, Intent data);

        void onImageCropped(Uri uri, File imageFile);
    }


    ///////////////FOR CAMERA///////////////////////


    public static void selectImage(final Activity activity) {

        activity.startActivityForResult(getFromCamera(activity), 9162);

    }

    private static Intent getFromCamera(final Activity activity) {
        Log.d(TAG, "getFromCamera: ");
        Uri outputFileUri = getCaptureImageOutputUri(activity);

        File file = new File(((Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) ? getRealPathFromURI_24_Greater(activity, outputFileUri) : getRealPathFromURI(activity, outputFileUri)));

        if (file.exists())
            file.delete();

        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (outputFileUri != null) {
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        }

        return captureIntent;
    }

    private static Uri getCaptureImageOutputUri(Activity activity) {
        Log.d(TAG, "getCaptureImageOutputUri: ");
        Uri outputFileUri = null;
        File getImage = activity.getExternalCacheDir();
        if (getImage != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                outputFileUri =
                        FileProvider.getUriForFile(activity,
                                MyApplication.APPLICATION_ID + ".provider",
                                new File(getImage.getPath(), imageId + "_" + imageTypeId + "_" + "ImagePicked.jpeg"));
            } else {
                outputFileUri = Uri.fromFile(new File(getImage.getPath(), imageId + "_" + imageTypeId + "_" + "ImagePicked.jpeg"));
            }


        }
        return outputFileUri;
    }

    public static String getRealPathFromURI(Activity activity, Uri contentUri) {
        Log.d(TAG, "getRealPathFromURI: ");
        String result;
        Cursor cursor = activity.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            //result = contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            //result = cursor.getString(idx);
            cursor.close();
        }

        result = contentUri.getPath();
        return result;
    }


    public static String getRealPathFromURI_24_Greater(Activity activity, Uri contentUri) {
        Log.d(TAG, "getRealPathFromURI_24_Greater: ");
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }

    public static Uri getPickImageResultUri(Activity activity, Intent data) {
        Log.d(TAG, "getPickImageResultUri: ");
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri(activity) : getImageUri(activity, ((Bitmap) data.getExtras().get("data")));
        // return isCamera ? getCaptureImageOutputUri(activity) : data.getData();

    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {

        Log.d(TAG, "getImageUri: ");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}