package rajiv.com.mapproject.application_utils.Permission;

import android.Manifest;
import android.app.Activity;

/**
 * Created by RajivLL on 06-Jun-18.
 */
public class CheckPermissionUtil {

    //private static final int LOCATION_PERMISSION_REQ_CODE = 200;
    public static final int WRITE_SD_REQ_CODE = 201;

    /*public static void checkLocation(Activity activity,
                                     PermissionUtil.ReqPermissionCallback callback) {
        PermissionUtil.checkPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION,
                LOCATION_PERMISSION_REQ_CODE,
                "We need location permission to locate your position",
                "We can't get your location without location permission",
                callback);
    }*/

    public static void checkWriteSd(Activity activity,
                                    PermissionUtil.ReqPermissionCallback callback, String reason, String message) {
        PermissionUtil.checkPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                WRITE_SD_REQ_CODE,
                //"We need write external storage permission to save your location to file",
                //"We can't save your location to file without storage permission",
                reason,
                message,
                callback);
    }

}
