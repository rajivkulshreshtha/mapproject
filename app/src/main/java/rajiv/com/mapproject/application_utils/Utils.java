package rajiv.com.mapproject.application_utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import rajiv.com.mapproject.application_utils.Permission.CheckPermissionUtil;
import rajiv.com.mapproject.application_utils.Permission.PermissionUtil;

/**
 * Created by RajivLL on 28-Aug-18.
 */

public class Utils {


    public static void replaceFragment(@NonNull Context context, @NonNull Fragment fragment,
                                       Bundle bundle, int frameId, boolean addToBackStack) {

        FragmentActivity fragmentActivity = (FragmentActivity) context;
        FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();

        if (bundle != null) {
            bundle.putAll(fragment.getArguments());
            fragment.setArguments(bundle);
        }

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment, "tag" + fragment.getClass().getSimpleName());

        if (addToBackStack) {
            transaction.addToBackStack("stack" + fragment.getClass().getSimpleName()); // <-- This makes magic!
        } else {
            transaction.disallowAddToBackStack();
        }

        transaction.commit();
    }

    public static void checkPhotoPermission(final Activity activity, final ImageInputHelper imageInputHelper, final String imageId, final String imageTypeId) {
        CheckPermissionUtil.checkWriteSd(activity,
                new PermissionUtil.ReqPermissionCallback() {
                    @Override
                    public void onResult(boolean success) {
                        if (success) {
                            takePhoto(imageInputHelper, imageId, imageTypeId);
                        }
                    }
                }, "We need storage permission to take photos."
                , "We can't take photos without storage permission");
    }

    private static void takePhoto(final ImageInputHelper imageInputHelper, final String imageId, final String imageTypeId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            imageInputHelper.takePhotoWithCamera_24_Greater(imageId, imageTypeId);
        } else {
            imageInputHelper.takePhotoWithCamera(imageId, imageTypeId);
        }

    }
}
